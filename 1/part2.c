#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(void)
{
    int numbers_length = 200;

    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int numbers[numbers_length];

    int index = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        sscanf(line, "%d", &numbers[index]);
        index++;
    }

    bool found = false;
    for (int i = 0; i < numbers_length && !found; i++)
    {
        for (int j = 0; j < numbers_length && !found; j++)
        {
            for (int k = 0; k < numbers_length && !found; k++)
            {

                if (numbers[i] + numbers[j] + numbers[k] == 2020)
                {
                    printf("found: %d, %d, %d: %d\n", numbers[i], numbers[j], numbers[k], numbers[i] * numbers[j] * numbers[k]);
                    found = true;
                }
            }
        }
    }

    fclose(fp);

    if (line)
        free(line);

    exit(EXIT_SUCCESS);
}
