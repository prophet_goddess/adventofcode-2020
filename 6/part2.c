#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_QUESTIONS 26

int main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int *group_yes_answers = calloc(NUM_QUESTIONS, sizeof(int));
    int total_yes_answers = 0;
    int group_size = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (line[0] == '\n')
        {
            for (int i = 0; i < NUM_QUESTIONS; i++)
            {
                if (group_yes_answers[i] == group_size)
                {
                    total_yes_answers++;
                }
            }

            free(group_yes_answers);
            group_yes_answers = calloc(NUM_QUESTIONS, sizeof(int));
            group_size = 0;
        }
        else
        {
            group_size++;
            int line_index = 0;
            while (line[line_index] != '\n')
            {
                int question = (int)line[line_index] - 97;
                group_yes_answers[question]++;
                line_index++;
            }
        }
    }

    printf("total yes answers: %d\n", total_yes_answers);
}