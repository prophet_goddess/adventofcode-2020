#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define NUM_SEATS 818

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void selection_sort(int arr[], int n)
{
    int i, j, min_idx;

    for (i = 0; i < n - 1; i++)
    {

        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;
        swap(&arr[min_idx], &arr[i]);
    }
}

int main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int seat_ids[NUM_SEATS] = {-1.0};
    int seat_index = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        int max_row = 127;
        int min_row = 0;
        int min_column = 0;
        int max_column = 7;
        int line_index = 0;
        char ch = line[line_index];

        while (ch != '\n')
        {
            if (ch == 'F')
            {
                max_row = (int)floor((min_row + max_row) / 2.0);
            }
            else if (ch == 'B')
            {
                min_row = (int)ceil((min_row + max_row) / 2.0);
            }
            else if (ch == 'R')
            {
                min_column = (int)ceil((min_column + max_column) / 2.0);
            }
            else if (ch == 'L')
            {
                max_column = (int)floor((min_column + max_column) / 2.0);
            }

            line_index++;
            ch = line[line_index];
        }

        int seat_id = min_row * 8 + min_column;
        seat_ids[seat_index] = seat_id;
        seat_index++;
    }

    selection_sort(seat_ids, NUM_SEATS);

    for (int i = 2; i < NUM_SEATS - 2; i++)
    {
        int a = seat_ids[i];
        int b = seat_ids[i - 1];
        if (a - b > 1)
        {
            printf("i: %d, a: %d, b: %d\n", i, a, b);
            printf("Your seat is %d\n", a - 1);
        }
    }
}
