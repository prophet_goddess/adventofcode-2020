#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    double largest = -1;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        double min_row = 0;
        double max_row = 127;
        double min_column = 0;
        double max_column = 7;
        int line_index = 0;
        char ch = line[line_index];

        while (ch != '\n')
        {
            if (ch == 'F')
            {
                max_row = floor((min_row + max_row) / 2.0);
            }
            else if (ch == 'B')
            {
                min_row = ceil((min_row + max_row) / 2.0);
            }
            else if (ch == 'R')
            {
                min_column = ceil((min_column + max_column) / 2.0);
            }
            else if (ch == 'L')
            {
                max_column = floor((min_column + max_column) / 2.0);
            }

            line_index++;
            ch = line[line_index];
        }

        int seat_id = min_row * 8 + min_column;
        if (seat_id > largest)
        {
            largest = seat_id;
        }
    }
    printf("largest seat id: %f\n", largest);
}
