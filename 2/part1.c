#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int valid_count = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        int line_index = 0;
        char *minimum = (char *)malloc(4 * sizeof(char));
        while (line[line_index] != '-')
        {
            minimum[line_index] = line[line_index];
            line_index++;
        }
        line_index++;

        char *maximum = (char *)malloc(4 * sizeof(char));
        int max_index = 0;
        while (line[line_index] != ' ')
        {
            maximum[max_index] = line[line_index];
            max_index++;
            line_index++;
        }
        line_index++;

        char required = line[line_index];

        line_index += 3;

        int min = 0;
        sscanf(minimum, "%d", &min);
        int max = 0;
        sscanf(maximum, "%d", &max);

        free(minimum);
        free(maximum);

        int required_count = 0;
        while (line[line_index] != '\0')
        {
            if (line[line_index] == required)
                required_count++;
            line_index++;
        }

        if (required_count >= min && required_count <= max)
        {
            valid_count++;
        }
    }

    printf("there are %d valid passwords\n", valid_count);

    return 0;
}
