#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int valid_count = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        int line_index = 0;
        char *position1 = (char *)malloc(4 * sizeof(char));
        while (line[line_index] != '-')
        {
            position1[line_index] = line[line_index];
            line_index++;
        }
        line_index++;

        char *position2 = (char *)malloc(4 * sizeof(char));
        int max_index = 0;
        while (line[line_index] != ' ')
        {
            position2[max_index] = line[line_index];
            max_index++;
            line_index++;
        }
        line_index++;

        char required = line[line_index];

        line_index += 3;

        int pos1 = 0;
        sscanf(position1, "%d", &pos1);
        free(position1);
        pos1 -= 1; //puzzle is indexed from 1

        int pos2 = 0;
        sscanf(position2, "%d", &pos2);
        free(position2);
        pos2 -= 1;

        char *password = (char *)malloc(32 * sizeof(char));
        int password_index = 0;
        while (line[line_index] != '\n')
        {
            password[password_index] = line[line_index];
            password_index++;
            line_index++;
        }

        if ((password[pos1] == required && password[pos2] != required) || (password[pos2] == required && password[pos1] != required))
            valid_count++;
        free(password);
    }

    printf("there are %d valid passwords\n", valid_count);

    return 0;
}
