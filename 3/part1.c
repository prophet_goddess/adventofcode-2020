#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int slope_right = 3;
    int slope_down = 1;
    int num_lines = 323;
    int line_length = 31;

    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    char *lines[num_lines];
    int line_num = 0;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        lines[line_num] = calloc(line_length, sizeof(char));
        if (lines[line_num] == NULL)
        {
            fprintf(stderr, "calloc failed\n");
            exit(EXIT_FAILURE);
        }
        strcpy(lines[line_num], line);
        line_num++;
    }

    int y = 0;
    int x = 0;
    int trees = 0;
    while (y < num_lines)
    {
        x += slope_right;
        x %= line_length;
        y += slope_down;
        if (lines[y][x] == '#')
            trees++;
    }

    printf("trees: %d\n", trees);

    exit(EXIT_SUCCESS);
}