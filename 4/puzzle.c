#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct Passport
{
    int birthYear;
    int issueYear;
    int expirationYear;
    char *height;
    char *hairColor;
    char *eyeColor;
    char *pid;
};

struct Passport make_passport()
{
    struct Passport temp;
    temp.birthYear = -1;
    temp.issueYear = -1;
    temp.expirationYear = -1;
    temp.height = NULL;
    temp.hairColor = NULL;
    temp.eyeColor = NULL;
    temp.pid = NULL;

    return temp;
}

void free_passport(struct Passport p)
{
    if (p.eyeColor != NULL)
    {
        free(p.eyeColor);
    }
    if (p.hairColor != NULL)
    {
        free(p.hairColor);
    }
    if (p.height != NULL)
    {
        free(p.height);
    }
    if (p.pid != NULL)
    {
        free(p.pid);
    }
}

int num_eyecolors = 7;
char *eyecolors[7] = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
bool is_valid_eyecolor(char *eyecolor)
{
    for (int i = 0; i < num_eyecolors; i++)
    {
        if (strcmp(eyecolors[i], eyecolor) == 0)
        {
            return true;
        }
    }
    return false;
}

char valid_hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
bool is_valid_haircolor(char *haircolor)
{
    int len = strlen(haircolor);
    if (haircolor[0] != '#' || len > 7)
    {
        return false;
    }

    for (int index = 1; index < len; index++)
    {
        bool valid = false;
        for (int hex = 0; hex < 16; hex++)
        {
            if (haircolor[index] == valid_hex[hex])
            {
                valid = true;
                break;
            }
        }
        if (!valid)
        {
            return false;
        }
    }

    return true;
}

int digits[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
bool is_valid_height(char *height)
{
    char *numbers = calloc(4, sizeof(char));
    int hgt;
    int index = 0;
    while (true)
    {
        bool is_digit = false;
        for (int i = 0; i < 10; i++)
        {
            if (height[index] == digits[i])
            {
                numbers[index] = height[index];
                is_digit = true;
                break;
            }
        }
        if (!is_digit)
        {
            break;
        }
        index++;
    }
    printf("numbers: %s\n", numbers);
    sscanf(numbers, "%d", &hgt);
    free(numbers);

    char *units = calloc(4, sizeof(char));
    int unit_index = 0;
    while (height[index] != '\0')
    {
        units[unit_index] = height[index];
        index++;
        unit_index++;
    }

    printf("Checking height %d with units %s\n", hgt, units);

    if (strcmp(units, "in") == 0)
    {
        free(units);
        bool valid = hgt >= 59 && hgt <= 76;
        return valid;
    }
    else if (strcmp(units, "cm") == 0)
    {
        free(units);
        bool valid = hgt >= 150 && hgt <= 193;
        return valid;
    }
    else
    {
        free(units);
        return false;
    }
}

main(int argc, char const *argv[])
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("./input", "r");

    if (fp == NULL)
        exit(EXIT_FAILURE);

    int valid = 0;
    struct Passport passport = make_passport();
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (line[0] != '\n' && line[0] != EOF && line[0] != '\0')
        {
            int line_index = 0;
            while (line[line_index] != '\n' && line[line_index] != EOF && line[line_index] != '\0')
            {
                char *field = calloc(3, sizeof(char));
                int field_index = 0;
                while (line[line_index] != ':')
                {
                    field[field_index] = line[line_index];
                    line_index++;
                    field_index++;
                }
                line_index++;

                if (strcmp(field, "byr") == 0)
                {
                    int birthYear;
                    char *byr = calloc(4, sizeof(char));
                    int byr_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        byr[byr_index] = line[line_index];
                        byr_index++;
                        line_index++;
                    }
                    sscanf(byr, "%d", &birthYear);
                    free(byr);
                    passport.birthYear = birthYear;
                }
                else if (strcmp(field, "iyr") == 0)
                {
                    int issueYear;
                    char *iyr = calloc(4, sizeof(char));
                    int iyr_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        iyr[iyr_index] = line[line_index];
                        iyr_index++;
                        line_index++;
                    }
                    sscanf(iyr, "%d", &issueYear);
                    free(iyr);
                    passport.issueYear = issueYear;
                }
                else if (strcmp(field, "eyr") == 0)
                {
                    int expirationYear;
                    char *eyr = calloc(4, sizeof(char));
                    int eyr_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        eyr[eyr_index] = line[line_index];
                        eyr_index++;
                        line_index++;
                    }
                    sscanf(eyr, "%d", &expirationYear);
                    free(eyr);
                    passport.expirationYear = expirationYear;
                }
                else if (strcmp(field, "hgt") == 0)
                {
                    passport.height = calloc(8, sizeof(char));
                    int hgt_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        passport.height[hgt_index] = line[line_index];
                        hgt_index++;
                        line_index++;
                    }
                }
                else if (strcmp(field, "hcl") == 0)
                {
                    passport.hairColor = calloc(8, sizeof(char));
                    int hcl_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        passport.hairColor[hcl_index] = line[line_index];
                        hcl_index++;
                        line_index++;
                    }
                }
                else if (strcmp(field, "ecl") == 0)
                {
                    passport.eyeColor = calloc(8, sizeof(char));
                    int ecl_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        passport.eyeColor[ecl_index] = line[line_index];
                        ecl_index++;
                        line_index++;
                    }
                }
                else if (strcmp(field, "pid") == 0)
                {
                    passport.pid = calloc(16, sizeof(char));
                    int pid_index = 0;
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        passport.pid[pid_index] = line[line_index];
                        pid_index++;
                        line_index++;
                    }
                }
                else
                {
                    while (line[line_index] != ' ' && line[line_index] != '\n' && line[line_index] != '\0')
                    {
                        line_index++;
                    }
                }

                free(field);
                line_index++;
            }
        }
        else
        {
            if (passport.birthYear != -1 && passport.birthYear >= 1920 && passport.birthYear <= 2002 &&
                passport.expirationYear != -1 && passport.expirationYear >= 2020 && passport.expirationYear <= 2030 && passport.issueYear != -1 && passport.issueYear >= 2010 && passport.issueYear <= 2020 && passport.pid != NULL && strlen(passport.pid) == 9 && passport.eyeColor != NULL && is_valid_eyecolor(passport.eyeColor) && passport.hairColor != NULL && is_valid_haircolor(passport.hairColor) && passport.height != NULL && is_valid_height(passport.height))
            {
                printf("valid passport!\n");
                valid++;
            }
            printf("new passport\n");
            free_passport(passport);
            passport = make_passport();
        }
    }

    free_passport(passport);

    printf("valid passports: %d\n", valid);
}
